#include "nf_fsm.h"
#include "nf_assert.h"

#include "string.h"

/* 检查信号状态并派发给状态机 */
void NF_FSM_CheckSignal(NF_FSM* me, NF_SignalName name)
{
    /* 对象指针不可为空 */
    NF_ASSERT( me != NF_NULL_PTR );

    me->State.Dispatch(me, name, NF_Signal_Get(name));
}

/* 转换状态机状态 */
void NF_FSM_Translate(NF_FSM* me, NF_State state) 
{
    /* 对象指针不可为空 */
    NF_ASSERT( me != NF_NULL_PTR );

    me->State = state;
}   

/* 语法糖，用于状态赋值 */
NF_State NF_FSM_State(NF_FSM_Handler handler)
{
	NF_State state;
	
	/* 函数指针不可为空 */
	NF_ASSERT(handler != NF_NULL_PTR);

	state.Dispatch = handler;
	return state;
}

/* 语法糖，用于判断状态名 */
NF_Bool NF_FSM_NameIs(NF_FSM_Name name1, NF_FSM_Name name2)
{
	/* 名称指针不可为空 */
	NF_ASSERT( (name1 != NF_NULL_PTR) && (name2 != NF_NULL_PTR) );

	if ( strcmp(name1, name2) == 0 )
	{
		return NF_Bool_True;
	} 
	else
	{
		return NF_Bool_False;
	}
}
