#ifndef NF_FSM_H
#define NF_FSM_H

#include "nf_type.h"
#include "nf_signal.h"

/* 既是状态变量，又是状态转换处理回调函数 */
typedef struct _nf_sta_machine NF_FSM;
typedef void (*NF_FSM_Handler)(NF_FSM* me, NF_SignalName name, NF_SignalValue val);

/* 名称类型定义 */
typedef NF_CString NF_FSM_Name;

/* 状态对象类型 */
typedef struct _nf_sta
{
    NF_FSM_Handler Dispatch;
#ifndef NF_NDEBUG
    NF_FSM_Name    Name;
#endif
}NF_State;


/* 状态机对象类型 */
typedef struct _nf_sta_machine
{
    NF_State     State;
#ifndef NF_NDEBUG
    NF_FSM_Name  Name;
#endif
}NF_FSM;

/* 状态转换宏 */
#define NF_FSM_TRAN(_sta) NF_FSM_Translate(me, NF_FSM_State(_sta))

/* 接口函数列表 */
extern void     NF_FSM_CheckSignal (NF_FSM* me, NF_SignalName name);       // 获取信号状态并派发给状态机
extern void     NF_FSM_Translate   (NF_FSM* me, NF_State state);           // 转换状态机状态
extern NF_State NF_FSM_State       (NF_FSM_Handler handler);               // 语法糖，用于状态赋值
extern NF_Bool  NF_FSM_NameIs      (NF_FSM_Name name1, NF_FSM_Name name2); // 语法糖，用于判断状态名

#endif