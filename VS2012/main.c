#include "n_frame.h"

#include "windows.h"
#include "stdio.h"

#define IS_KEY_PRESS(_key) ((GetKeyState(_key) >= 0) ? NF_Bool_False : NF_Bool_True )

/* 信号产生者 */
void Test_Key_Process(void)
{
	if ( IS_KEY_PRESS('Q') ){
		NF_Signal_Set("key_q_press", 1);
	}
	else{
		NF_Signal_Set("key_q_press", 0);
	}

	if ( IS_KEY_PRESS('E') ){
		NF_Signal_Set("key_e_press", 1);
	}
	else{
		NF_Signal_Set("key_e_press", 0);
	}
}

/* 状态机对象 */
NF_FSM Test_FSM_QandE;

/* 状态机的三个状态处理函数 */
void Test_FSM_QandE_IDLE(NF_FSM* me, NF_SignalName name, NF_SignalValue val);
void Test_FSM_QandE_QDOWN(NF_FSM* me, NF_SignalName name, NF_SignalValue val);
void Test_FSM_QandE_QEDOWN(NF_FSM* me, NF_SignalName name, NF_SignalValue val);

/* IDLE状态处理函数 */
void Test_FSM_QandE_IDLE(NF_FSM* me, NF_SignalName name, NF_SignalValue val)
{
	if ( NF_FSM_NameIs(name, "key_q_press") )
	{
		if ( val == 1 )
		{
			NF_FSM_TRAN(Test_FSM_QandE_QDOWN);
			printf("Test_FSM_QandE State Translate : IDLE --> QDOWN\n");
		}
	}
}

/* QDOWN状态处理函数 */
void Test_FSM_QandE_QDOWN(NF_FSM* me, NF_SignalName name, NF_SignalValue val)
{
	if ( NF_FSM_NameIs(name, "key_e_press") )
	{
		if ( val == 1 )
		{
			NF_FSM_TRAN(Test_FSM_QandE_QEDOWN);
			printf("Test_FSM_QandE State Translate : QDOWN --> QEDOWN\n");
		}
	}
	else if( NF_FSM_NameIs(name, "key_q_press") )
	{
		if ( val == 0 )
		{
			NF_FSM_TRAN(Test_FSM_QandE_IDLE);
			printf("Test_FSM_QandE State Translate : QDOWN --> IDLE\n");
		}
	}
}

/* QEDOWN状态处理函数 */
void Test_FSM_QandE_QEDOWN(NF_FSM* me, NF_SignalName name, NF_SignalValue val)
{
	if ( NF_FSM_NameIs(name, "key_e_press") )
	{
		if ( val == 0 )
		{
			NF_FSM_TRAN(Test_FSM_QandE_QDOWN);
			printf("Test_FSM_QandE State Translate : QEDOWN --> QDOWN\n");
		}
	}
	else if( NF_FSM_NameIs(name, "key_q_press") )
	{
		if ( val == 0 )
		{
			NF_FSM_TRAN(Test_FSM_QandE_IDLE);
			printf("Test_FSM_QandE State Translate : QEDOWN --> IDLE\n");
		}
	}
}

int main(void)
{
	/* 初始化状态机 */
	NF_FSM_Translate(&Test_FSM_QandE, NF_FSM_State(Test_FSM_QandE_IDLE));

	for (;;)
	{
		Test_Key_Process();
		NF_FSM_CheckSignal(&Test_FSM_QandE, "key_q_press");
		NF_FSM_CheckSignal(&Test_FSM_QandE, "key_e_press");
	}
}