# NorthFrame  单片机极简信号/状态机框架  
# 讨论QQ群：431600056

单片机开发中是否被满天飞的全局变量困扰？  
是否在寻找一种层级间松耦合的信号传递方式？  
是否希望优雅无负担地使用状态机思路进行开发？  
调试打Log很麻烦，希望自动生成，最好还有测试脚本？  
#### NF_Signal和NF_FSM 来帮你

### 用法1：代替全局变量，使用方便  
```
NF_Signal_Set("flag_connect", 1);
NF_Signal_Set("blink_cnt", 3);

NF_SignalValue flag_connect = NF_Signal_Get("flag_connect");
```


### 用法2：配合状态机使用  
以下例程在VS2012环境中运行一个判断QE组合键的状态机   
注：未来会更新使用状态图绘图软件自动生成状态机代码 
![QandE_FSM](/QandE_FSM.jpg)  

```
#include "n_frame.h"

#include "windows.h"
#include "stdio.h"

#define IS_KEY_PRESS(_key) ((GetKeyState(_key) >= 0) ? NF_Bool_False : NF_Bool_True )

/* 信号产生者 */
void Test_Key_Process(void)
{
	if ( IS_KEY_PRESS('Q') ){
		NF_Signal_Set("key_q_press", 1);
	}
	else{
		NF_Signal_Set("key_q_press", 0);
	}

	if ( IS_KEY_PRESS('E') ){
		NF_Signal_Set("key_e_press", 1);
	}
	else{
		NF_Signal_Set("key_e_press", 0);
	}
}

/* 状态机对象 */
NF_FSM Test_FSM_QandE;

/* 状态机的三个状态处理函数 */
void Test_FSM_QandE_IDLE(NF_FSM* me, NF_SignalName name, NF_SignalValue val);
void Test_FSM_QandE_QDOWN(NF_FSM* me, NF_SignalName name, NF_SignalValue val);
void Test_FSM_QandE_QEDOWN(NF_FSM* me, NF_SignalName name, NF_SignalValue val);

/* IDLE状态处理函数 */
void Test_FSM_QandE_IDLE(NF_FSM* me, NF_SignalName name, NF_SignalValue val)
{
	if ( NF_FSM_NameIs(name, "key_q_press") )
	{
		if ( val == 1 )
		{
			NF_FSM_TRAN(Test_FSM_QandE_QDOWN);
			printf("Test_FSM_QandE State Translate : IDLE --> QDOWN\n");
		}
	}
}

/* QDOWN状态处理函数 */
void Test_FSM_QandE_QDOWN(NF_FSM* me, NF_SignalName name, NF_SignalValue val)
{
	if ( NF_FSM_NameIs(name, "key_e_press") )
	{
		if ( val == 1 )
		{
			NF_FSM_TRAN(Test_FSM_QandE_QEDOWN);
			printf("Test_FSM_QandE State Translate : QDOWN --> QEDOWN\n");
		}
	}
	else if( NF_FSM_NameIs(name, "key_q_press") )
	{
		if ( val == 0 )
		{
			NF_FSM_TRAN(Test_FSM_QandE_IDLE);
			printf("Test_FSM_QandE State Translate : QDOWN --> IDLE\n");
		}
	}
}

/* QEDOWN状态处理函数 */
void Test_FSM_QandE_QEDOWN(NF_FSM* me, NF_SignalName name, NF_SignalValue val)
{
	if ( NF_FSM_NameIs(name, "key_e_press") )
	{
		if ( val == 0 )
		{
			NF_FSM_TRAN(Test_FSM_QandE_QDOWN);
			printf("Test_FSM_QandE State Translate : QEDOWN --> QDOWN\n");
		}
	}
	else if( NF_FSM_NameIs(name, "key_q_press") )
	{
		if ( val == 0 )
		{
			NF_FSM_TRAN(Test_FSM_QandE_IDLE);
			printf("Test_FSM_QandE State Translate : QEDOWN --> IDLE\n");
		}
	}
}

int main(void)
{
	/* 初始化状态机 */
	NF_FSM_Translate(&Test_FSM_QandE, NF_FSM_State(Test_FSM_QandE_IDLE));

	for (;;)
	{
		Test_Key_Process();
		NF_FSM_CheckSignal(&Test_FSM_QandE, "key_q_press");
		NF_FSM_CheckSignal(&Test_FSM_QandE, "key_e_press");
	}
}
```